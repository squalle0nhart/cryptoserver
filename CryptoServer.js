const request = require('request');
const jsdom = require('jsdom');
//const firebaseFunc = require('firebase-functions');
const { JSDOM } = jsdom;
const image2base64 = require('image-to-base64');

const CRYPTO_API_KEY = '0f3e89f66f84e473b9659845d7de80f13ef16b45deffce96c2f79335e475ed90';
const API_ALL_COIN = 'https://min-api.cryptocompare.com/data/all/coinlist';
const COIN_MARKET_CAP = 'https://coinmarketcap.com/all/views/all/';

//******************************************************************************************************************/
const admin = require('firebase-admin');
function initFireStore() {
    var serviceAccount = require('../server/cryptoserver_service_account.json');
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount)
    });
}

function getAllCoinLists(isUpdateIcon) {
    initFireStore();
    const db = admin.firestore();
    const settings = { timestampsInSnapshots: true };
    db.settings(settings);
    JSDOM.fromURL(COIN_MARKET_CAP).then(async function (dom) {
        const listCoin = dom.window.document.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
        for (let i = 0; i < listCoin.length; i++) {
            try {
                let element = listCoin[i];
                let coinItem = {};
                coinItem.id = element.getAttribute('id').replace('id-', '');
                if (element.getElementsByClassName('text-center')[0]) {
                    coinItem.rank = element.getElementsByClassName('text-center')[0].textContent.trim();
                }

                coinItem.name = "null";
                if (element.getElementsByClassName('currency-name-container link-secondary')[0]) {
                    coinItem.name = element.getElementsByClassName('currency-name-container link-secondary')[0].textContent.trim();
                }

                coinItem.symbolText = "null";
                if (element.getElementsByClassName('text-left col-symbol')[0]) {
                    coinItem.symbolText = element.getElementsByClassName('text-left col-symbol')[0].textContent.trim();
                }

                if (element.getElementsByClassName('no-wrap market-cap text-right')[0]) {
                    coinItem.marketCapUSD = element.getElementsByClassName('no-wrap market-cap text-right')[0].textContent.trim();
                }

                coinItem.priceUSD = "null";
                if (element.getElementsByClassName('price')[0]) {
                    coinItem.priceUSD = element.getElementsByClassName('price')[0].textContent.trim();
                }

                coinItem.volumeUSD = "null";
                if (element.getElementsByClassName('volume')[0]) {
                    coinItem.volumeUSD = element.getElementsByClassName('volume')[0].textContent.trim();
                }

                coinItem.priceUSD = "null";
                if (element.getElementsByClassName('price')[0]) {
                    coinItem.priceUSD = element.getElementsByClassName('price')[0].textContent.trim();
                }

                coinItem.priceChange1H = "null";
                if (element.querySelector('[data-timespan="1h"]')) {
                    coinItem.priceChange1H = element.querySelector('[data-timespan="1h"]').textContent.trim();
                }

                coinItem.priceChange24H = "null";
                if (element.querySelector('[data-timespan="24h"]')) {
                    coinItem.priceChange24H = element.querySelector('[data-timespan="24h"]').textContent.trim();
                }

                coinItem.priceChange7D = "null";
                if (element.querySelector('[data-timespan="7d"]')) {
                    coinItem.priceChange7D = element.querySelector('[data-timespan="7d"]').textContent.trim();
                }

                if (isUpdateIcon) {
                    coinItem.img = "null";
                    if (element.querySelector('[data-cc-id]')) {
                        let imgSrc = element.querySelector('[data-cc-id]').getAttribute('data-cc-id').trim();
                        imgSrc = 'https://s2.coinmarketcap.com/static/img/coins/64x64/' + imgSrc + '.png';
                        const imgBase64 = await getImgFromURL(imgSrc);
                        coinItem.img = imgBase64;
                    }
                }
                db.collection('coinlist').doc(coinItem.id).set(coinItem);
                console.log('OK: ' + i);
            } catch (error) {
                console.log(error);
                console.log('error: ' + i);
                continue;
            }
        }
        console.log('complete')
    });
}

function getImgFromURL(imgSrc) {
    return image2base64(imgSrc);
}
getAllCoinLists(true);